/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import {createStackNavigator} from 'react-navigation';
import React, {  } from 'react';
import {Login} from "./views/Login";
import {Workspaces} from "./views/Workspaces";
import {Folder} from "./views/Folder";
import {Viewer} from "./views/Viewer";

const App = createStackNavigator({
    Login: { screen: Login },
    Workspaces: { screen: Workspaces },
    Folder: { screen: Folder },
    Viewer: { screen: Viewer }
});

export default App;
